{.experimental: "strictFuncs".}

import
  conway,
  illwill,
  unicode,
  os

type
  Game* = ref object
    grid: Grid2d
    term: TerminalBuffer

func newGame*(w, h: Natural): Game {.inline.} =
  Game(
    grid: initGrid2d(w, h),
    term: newTerminalBuffer(w, h),
  )

func newTerminalChar(r: string, fg = fgWhite, bg = bgNone,
                     style: set[Style] = {}) : TerminalChar {.inline.} =
  TerminalChar(ch: r.runeAt(0), fg: fg, bg: bg, style: style)

proc cursorUp(tb: var TerminalBuffer, count: Natural = 1) =
  let y = tb.getCursorYPos()
  let offset = count mod tb.height()
  let yp = if y - offset < 0:
    tb.height() + (y - offset)
  else:
    y - offset
  tb.setCursorYPos(yp)

proc cursorDown(tb: var TerminalBuffer, count: Natural = 1) =
  let y = tb.getCursorYPos()
  tb.setCursorYPos((y + count) mod tb.height())

proc cursorBackward(tb: var TerminalBuffer, count: Natural = 1) =
  let x = tb.getCursorXPos()
  let offset = count mod tb.width()
  let xp = if x - offset < 0:
    tb.width() + (x - offset)
  else:
    x - offset
  tb.setCursorXPos(xp)

proc cursorForward(tb: var TerminalBuffer, count: Natural = 1) =
  let x = tb.getCursorXPos()
  tb.setCursorXPos((x + count) mod tb.width())

func display*(g: var Game, ch = "O") =
  let mark = newTerminalChar(ch, fg=fgBlue);
  let blank = newTerminalChar(" ", fg=fgBlue);
  for i in 0..<g.grid.height():
    for j in 0..<g.grid.width():
      if g.grid[j, i]:
        g.term[j, i] = mark
      else:
        g.term[j, i] = blank

proc edit*(g: var Game) =
  g.term.clear()
  showCursor()
  g.term.setCursorPos(g.term.width() div 2, g.term.height() div 2)
  while true:
    let k = getKey()
    case k
    of Key.H:
      g.term.cursorBackward()
    of Key.L:
      g.term.cursorForward()
    of Key.K:
      g.term.cursorUp()
    of Key.J:
      g.term.cursorDown()
    of Key.Space:
      let (x, y) = g.term.getCursorPos()
      g.grid[x, y] = not g.grid[x, y]
      g.display()
    of Key.Q:
      hideCursor()
      return
    else:
      discard
    g.term.display()
    sleep(20)

proc play*(g: var Game) =
  while true:
    let k = getKey()
    case k
    of Key.Space:
      g.grid.step()
      g.display()
    of Key.Q:
      return
    else: discard
    g.term.display()
    sleep(20)
