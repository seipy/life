{.experimental: "strictFuncs".}

import random

type
  Grid2d* = object
    markedCells: seq[bool]
    cells: seq[bool]
    width: Natural
    height: Natural
    aliveCells: Natural

func initGrid2d*(width, height: Natural): Grid2d {.inline.} =
  Grid2d(
    markedCells: newSeq[bool](width * height),
    cells: newSeq[bool](width * height),
    width: width,
    height: height,
    aliveCells: 0.Natural
  )

func width*(g: Grid2d): Natural {.inline.} = g.width

func height*(g: Grid2d): Natural {.inline.} = g.height

func aliveCells*(g: Grid2d): Natural {.inline.} = g.aliveCells

func `[]`*(g: Grid2d, x, y: int): lent bool {.inline.} =
  g.cells[y * g.width + x]

func `[]=`*(g: var Grid2d, x, y: int, v: sink bool) =
  if g.cells[y * g.width + x] != v:
    if v:
      inc g.aliveCells
    else:
      dec  g.aliveCells
  g.cells[y * g.width + x] = v

func numNeis*(g: Grid2d, x, y: Natural): int =
  result = 0
  for j in [x-1, x, x+1]:
    for i in [y-1, y, y+1]:
      if(j, i) != (x, y):
        let jp = (if j > 0: j else: j + g.width) mod g.width
        let ip = (if i > 0: i else: i + g.height) mod g.height
        if g[jp, ip]:
          inc result

proc randomize*(g: var Grid2d, seed=666) =
  var r = initRand(seed)
  for i in 0..<g.height:
    for j in 0..<g.width:
      g[j, i] = r.sample([true, false])

func mark(g: var Grid2d, x, y:Natural, v: bool = true) {.inline.} =
  g.markedCells[y * g.width + x] = v

func isMarked(g: var Grid2d, x, y: Natural) : bool {.inline.} =
  g.markedCells[y * g.width + x]

proc step*(g: var Grid2d) =
  for i in 0..<g.height:
    for j in 0..<g.width:
      let n = g.numNeis(j, i)
      if g[j, i] and (n < 2 or n > 3):
        g.mark(j, i)
      elif n == 3:
        g.mark(j, i)

  for i in 0..<g.height:
    for j in 0..<g.width:
      if g.isMarked(j, i):
        g[j, i] = not g[j, i]
        g.mark(j, i, false)
