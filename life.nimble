# Package

version       = "0.1.0"
author        = "Edgar Quiroz"
description   = "Terminal based game of life"
license       = "MIT"
srcDir        = "src"
binDir        = "bin"
bin           = @["life"]

# tasks
task projectName, "project info":
  echo $projectDir()

# Dependencies

requires "nim >= 1.5.1"
requires "illwill"
