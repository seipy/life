import
  tui,
  illwill

proc exitProc() {.noconv.} =
  illwillDeinit()
  showCursor()
  quit(0)

proc initProc() =
  illwillInit(fullscreen=false)
  setControlCHook(exitProc)
  hideCursor()

when isMainModule:
  initProc()
  var g = newGame(50, 20)
  g.display()

  g.edit()
  g.play()

  exitProc()
